@echo off
rem ======================================================================
rem windows startup script
rem
rem author: dingdangmaoup
rem date: 2021-10-28
rem ======================================================================

rem Open in a browser
start "" "http://localhost:8089/"

rem startup jar
java -jar ../boot/demo-1.0.0.jar --spring.config.location=../config/

pause