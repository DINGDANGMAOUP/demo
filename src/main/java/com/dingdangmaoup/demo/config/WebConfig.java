//package com.dingdangmaoup.demo.config;
//
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//public class WebConfig implements WebMvcConfigurer {
//
//
//  @Override
//  public void addResourceHandlers(ResourceHandlerRegistry registry) {
//    // 解决静态资源无法访问  如果你的静态文件在这两个文件夹下面都有 此处两个都需要
//    registry.addResourceHandler("/templates/**")
//        .addResourceLocations("classpath:/templates/");
//    registry.addResourceHandler("/static/**")
//        .addResourceLocations("classpath:/static/");
//  }
//
//}
