package com.dingdangmaoup.demo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author dzhao1
 */
@Controller
@RequestMapping("/")
public class IndexController {
  @Value("${msg}")
  private String msg;
  @GetMapping("index")
  public String index(){
    return "index";
  }
  @GetMapping("test")
  @ResponseBody
  public String test(){
    return msg;
  }

}
